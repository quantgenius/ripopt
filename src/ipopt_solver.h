#ifndef _R_IPOPT_SOLVER_
#define _R_IPOPT_SOLVER_

#include <IpIpoptApplication.hpp>

#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>



extern "C"
{
    SEXP ipopt_solve(SEXP num_var_S, 
                     SEXP var_types_S,
                     SEXP x_init_S, 
                     SEXP x_lb_S, 
                     SEXP x_ub_S,
                     SEXP init_bm_low_S,
                     SEXP init_bm_high_S,
                     SEXP num_nonlin_S,
                     SEXP nonlin_idx_S,

                     SEXP num_constr_S, 
                     SEXP constr_type_S,
                     SEXP constr_lb_S, 
                     SEXP constr_ub_S,
                     SEXP init_lambda_S,

                     SEXP nnz_jac_S, 
                     SEXP nnz_hess_S, 

                     SEXP obj_scale_S,
                     SEXP x_scale_S,
                     SEXP g_scale_S,

                     SEXP objFn, 
                     SEXP gradFn, 
                     SEXP constrFn, 
                     SEXP jacFn, 
                     SEXP jacIdx, 
                     SEXP hessFn, 
                     SEXP hessIdx, 
                     SEXP env,
        
                     SEXP options,
                     SEXP option_types);
   
};
SEXP ipopt_solve_cpp(SEXP num_var_S, 
                     SEXP var_types_S,
                     SEXP x_init_S, 
                     SEXP x_lb_S, 
                     SEXP x_ub_S,
                     SEXP init_bm_low_S,
                     SEXP init_bm_high_S,
                     SEXP num_nonlin_S,
                     SEXP nonlin_idx_S,

                     SEXP num_constr_S, 
                     SEXP constr_type_S,
                     SEXP constr_lb_S, 
                     SEXP constr_ub_S,
                     SEXP init_lambda_S,

                     SEXP nnz_jac_S, 
                     SEXP nnz_hess_S, 

                     SEXP obj_scale_S,
                     SEXP x_scale_S,
                     SEXP g_scale_S,

                     SEXP objFn, 
                     SEXP gradFn, 
                     SEXP constrFn, 
                     SEXP jacFn, 
                     SEXP jacIdx, 
                     SEXP hessFn, 
                     SEXP hessIdx, 
                     SEXP env,
        
                     SEXP options,
                     SEXP option_types);


bool set_options(SEXP options, SEXP option_types, SmartPtr< IpoptApplication >& app);

#endif
