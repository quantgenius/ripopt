#ifndef _R_NLP_
#define _R_NLP_

#include <IpTNLP.hpp>
#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>

using namespace Ipopt;

class R_NLP: public Ipopt::TNLP
{
public:
R_NLP(SEXP num_var_S, 
      SEXP var_types_S,
      SEXP x_init_S, 
      SEXP x_lb_S, 
      SEXP x_ub_S,
      SEXP init_bm_low_S,
      SEXP init_bm_high_S,
      SEXP num_nonlin_S,
      SEXP nonlin_idx_S,

      SEXP num_constr_S, 
      SEXP constr_type_S,
      SEXP constr_lb_S, 
      SEXP constr_ub_S,
      SEXP init_lambda_S,

      SEXP nnz_jac_S, 
      SEXP nnz_hess_S, 

      SEXP obj_scale_S,
      SEXP x_scale_S,
      SEXP g_scale_S,

      SEXP objFn, 
      SEXP gradFn, 
      SEXP constrFn, 
      SEXP jacFn, 
      SEXP jacIdx, 
      SEXP hessFn, 
      SEXP hessIdx, 
      SEXP env,
      
      SEXP* retval);


    virtual ~R_NLP();

    /* Method to return some info about the nlp */
    virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                            Index& nnz_h_lag, IndexStyleEnum& index_style);

    /** overload this method to return scaling parameters. This is
     *  only called if the options are set to retrieve user scaling.
     *  There, use_x_scaling (or use_g_scaling) should get set to true
     *  only if the variables (or constraints) are to be scaled.  This
     *  method should return true only if the scaling parameters could
     *  be provided.
     */
    virtual bool get_scaling_parameters(Number& obj_scaling,
                                        bool& use_x_scaling, Index n,
                                        Number* x_scaling,
                                        bool& use_g_scaling, Index m,
                                        Number* g_scaling);

    /** overload this method to return the variables linearity
     * (TNLP::Linear or TNLP::NonLinear). The var_types
     *  array should be allocated with length at least n. (default implementation
     *  just return false and does not fill the array).*/
    virtual bool get_variables_linearity(Index n, LinearityType* var_types);

    /** overload this method to return the constraint linearity.
     * array should be alocated with length at least n. (default implementation
     *  just return false and does not fill the array).*/
    virtual bool get_constraints_linearity(Index m, LinearityType* const_types);


    /* Method to return the bounds for my problem */
    virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                               Index m, Number* g_l, Number* g_u);

    /* Method to return the starting point for the algorithm */
    virtual bool get_starting_point(Index n, bool init_x, Number* x,
                                  bool init_z, Number* z_L, Number* z_U,
                                  Index m, bool init_lambda,
                                  Number* lambda);

    /* Method to return the objective value */
    virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);

    /* Method to return the gradient of the objective */
    virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);

    /* Method to return the constraint residuals */
    virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);

    /** Method to return:
    *   1) The structure of the jacobian (if "values" is NULL)
    *   2) The values of the jacobian (if "values" is not NULL)
    */
    virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
                            Index m, Index nele_jac, Index* iRow, Index *jCol,
                            Number* values);

    /** Method to return:
    *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
    *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
    */
    virtual bool eval_h(Index n, const Number* x, bool new_x,
                      Number obj_factor, Index m, const Number* lambda,
                      bool new_lambda, Index nele_hess, Index* iRow,
                      Index* jCol, Number* values);

    /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
    virtual void finalize_solution(SolverReturn status,
                                   Index n, const Number* x, const Number* z_L, const Number* z_U,
                                   Index m, const Number* g, const Number* lambda,
                                   Number obj_value,
				   const IpoptData* ip_data,
				   IpoptCalculatedQuantities* ip_cq);

    /** Intermediate Callback method for the user.  Providing dummy
     *  default implementation.  For details see IntermediateCallBack
     *  in IpNLP.hpp. */
    virtual bool intermediate_callback(AlgorithmMode mode,
                                       Index iter, Number obj_value,
                                       Number inf_pr, Number inf_du,
                                       Number mu, Number d_norm,
                                       Number regularization_size,
                                       Number alpha_du, Number alpha_pr,
                                       Index ls_trials,
                                       const IpoptData* ip_data,
                                       IpoptCalculatedQuantities* ip_cq);

    /** @name Methods for quasi-Newton approximation.  If the second
     *  derivatives are approximated by Ipopt, it is better to do this
     *  only in the space of nonlinear variables.  The following
     *  methods are call by Ipopt if the quasi-Newton approximation is
     *  selected.  If -1 is returned as number of nonlinear variables,
     *  Ipopt assumes that all variables are nonlinear.  Otherwise, it
     *  calls get_list_of_nonlinear_variables with an array into which
     *  the indices of the nonlinear variables should be written - the
     *  array has the lengths num_nonlin_vars, which is identical with
     *  the return value of get_number_of_nonlinear_variables().  It
     *  is assumed that the indices are counted starting with 1 in the
     *  FORTRAN_STYLE, and 0 for the C_STYLE. */
    virtual Index get_number_of_nonlinear_variables();

    virtual bool get_list_of_nonlinear_variables(Index num_nonlin_vars, 
                                                 Index* pos_nonlin_vars);

private:
    /*  Methods to block default compiler methods.
     * The compiler automatically generates the following three methods.
     *  Since the default compiler implementation is generally not what
     *  you want (for all but the most simple classes), we usually 
     *  put the declarations of these methods in the private section
     *  and never implement them. This prevents the compiler from
     *  implementing an incorrect "default" behavior without us
     *  knowing.    
     */
    R_NLP(const R_NLP&);
    R_NLP& operator=(const R_NLP&);

    /* SEXPs from R that are needed for callback */
    SEXP objFn_g;
    SEXP gradFn_g;
    SEXP constrFn_g;
    SEXP jacFn_g;
    SEXP jacIdx_g;
    SEXP hessFn_g;
    SEXP hessIdx_g;
    SEXP env_g;

    SEXP *retval_ptr;

    size_t num_var;
    int    *var_type;
    SEXP x_init_g;
    double *x_lb;
    double *x_ub;
    SEXP init_bm_low_g;
    SEXP init_bm_high_g;

    int num_nonlin;
    int *nonlin_idx;

    size_t num_constr;
    int    *constr_type;
    SEXP init_lambda_g;
    double *constr_lb;
    double *constr_ub;

    size_t nnz_jac;
    size_t nnz_hess;
    SEXP obj_scale_g;
    SEXP x_scale_g;
    SEXP g_scale_g;

    double *lambda_g;

    double* solution; 
    double* sol_bm_low; 
    double* sol_bm_high;
    double* sol_constr_val; 
    double* sol_lambda;
    double  sol_objval; 
    Ipopt::SolverReturn sol_retval;
    bool    solved;
};

#endif
