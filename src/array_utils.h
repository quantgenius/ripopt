#ifndef __ARRAY_DELETER__
#define __ARRAY_DELETER__

#include <cstdlib>


template< typename T >
T *allocate_array(size_t size)
{
    void *untyped_space = malloc(sizeof(T) * size);
    T *typed_space = static_cast< T* >(untyped_space);

    return typed_space;
};

template< typename T >
void deallocate_array(T *space)
{
    free(space);
};





template< typename T >
class array_deleter
{
public:
    array_deleter(T* ptr);
    ~array_deleter();

private:
    T *owned_ptr__;
};

template < typename T >
array_deleter< T >::array_deleter(T* ptr)
    :
    owned_ptr__(ptr)
{
};

template < typename T >
array_deleter< T >::~array_deleter()
{
    deallocate_array(owned_ptr__); 
    owned_ptr__ = NULL;
}




template< typename T >
class deleter
{
public:
    deleter(T* ptr);
    ~deleter();

private:
    T *owned_ptr__;
};

template < typename T >
deleter< T >::deleter(T* ptr)
    :
    owned_ptr__(ptr)
{
};

template < typename T >
deleter< T >::~deleter()
{
    delete owned_ptr__;
    owned_ptr__ = NULL;
}





enum layout_t{col_major = 101, row_major = 102};

inline size_t array_idx(size_t row_num, size_t col_num, size_t num_rows, size_t num_cols, layout_t layout)
{
    size_t idx = 0;
    if(col_major == layout)
    {
        idx = (col_num * num_rows) + row_num;
    }
    else
    { idx = (row_num * num_cols) + col_num; }
    return idx;
};



#endif
