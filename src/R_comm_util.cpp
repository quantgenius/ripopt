#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>

#include "R_comm_util.h"


SEXP mkans(double *x, size_t n)
{
    SEXP ans;
    PROTECT(ans = allocVector(REALSXP, n));
    for(size_t i=0; i != n; ++i)
    {
        REAL(ans)[i] = x[i];
    }
    UNPROTECT(1);
    return ans;
}

double feval(double *x, size_t n, SEXP f, SEXP rho)
{
    defineVar(install("x"), mkans(x, n), rho);
    return(REAL(eval(f, rho))[0]);
}

SEXP feval_SEXP(double *x, size_t n, SEXP f, SEXP rho)
{
    defineVar(install("x"), mkans(x, n), rho);
    SEXP res;
    PROTECT(res = eval(f, rho));
    UNPROTECT(1);
    return res;
}


int feval_n(double *x, size_t n, 
    double *ret_val, size_t m, 
    SEXP f, SEXP rho)
{
    defineVar(install("x"), mkans(x, n), rho);
    double m_dbl = m;
    defineVar(install("m"), mkans(&m_dbl, 1), rho);
    SEXP res;
    PROTECT(res = eval(f, rho));

    for(size_t i = 0; i != m; ++i)
    {
        if(ISNA(REAL(res)[i]))
        {
            UNPROTECT(1);
            return 1;
        }
        ret_val[i] = REAL(res)[i];
    }
    UNPROTECT(1);
    return 0; 
}


int feval_ij(double *x, size_t i, 
    double *y, size_t j, 
    double *ret, size_t m, 
    SEXP f, SEXP rho)
{
    defineVar(install("x"), mkans(x, i), rho);
    defineVar(install("y"), mkans(y, j), rho);

    double m_dbl = m;
    defineVar(install("m"), mkans(&m_dbl, 1), rho);

    SEXP res;
    PROTECT(res = eval(f, rho));
    for(size_t iter = 0; iter != m; ++iter)
    {
        if(ISNA(REAL(res)[iter]))
        {
            UNPROTECT(1);
            return 1;
        }
        ret[iter] = REAL(res)[iter];
    }
    UNPROTECT(1);
    return 0;
}


int feval_ijk(double *x, size_t i, 
    double *y, size_t j, 
    double *z, size_t k,
    double *ret, size_t m, 
    SEXP f, SEXP rho)
{
    defineVar(install("x"), mkans(x, i), rho);
    defineVar(install("y"), mkans(y, j), rho);
    defineVar(install("z"), mkans(z, k), rho);

    double m_dbl = m;
    defineVar(install("m"), mkans(&m_dbl, 1), rho);

    SEXP res;
    PROTECT(res = eval(f, rho));
    for(size_t iter = 0; iter != m; ++iter)
    {
        if(ISNA(REAL(res)[iter]))
        {
            UNPROTECT(1);
            return 1;
        }
        ret[iter] = REAL(res)[iter];
    }
    UNPROTECT(1);
    return 0;
}



int feval_idx(int *idx1, int *idx2, size_t len, SEXP f, SEXP rho)
{
    double len_dbl = len;
    defineVar(install("len"), mkans(&len_dbl, 1), rho);

    SEXP res;
    PROTECT(res = eval(f, rho));
    for(size_t iter = 0; iter != len; ++iter)
    {
        if(ISNA(INTEGER(res)[iter]))
        {
            UNPROTECT(1);
            return 1;
        }
        idx1[iter] = INTEGER(res)[iter];
    }

    for(size_t iter = 0; iter != len; ++iter)
    {
        if(ISNA(INTEGER(res)[iter+len]))
        {
            UNPROTECT(1);
            return 1;
        }
        idx2[iter] = INTEGER(res)[iter+len];
    }
    UNPROTECT(1);
    return 0;
}


SEXP get_list_element(SEXP list, const char* str)
{
    SEXP elmt  = R_NilValue;
    SEXP names = getAttrib(list, R_NamesSymbol);

    int list_length = length(list);
    for(int i = 0; i != list_length; ++i)
    {
        if(0 == strcmp(CHAR(STRING_ELT(names, i)), str))
        {
            elmt = VECTOR_ELT(list, i);
            break;
        }
    }
    return elmt;
}


