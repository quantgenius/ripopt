#ifndef _R_COMMUNICATION_UTILS_
#define _R_COMMUNICATION_UTILS_

#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>


// Make an SEXP of length n with data in x
SEXP mkans(double *x, size_t n);

// Evaluate a function f that takes a single argument x,
// x is a vector of length n and returns a scalar double
// Evaluate in environment rho
double feval(double *x, size_t n, SEXP f, SEXP rho);

// Evaluate a function f that takes a single argument x
// x is a vector of length n.  the function returns a
// general R sexp. Evaluate in environment rho
SEXP feval_SEXP(double *x, size_t n, SEXP f, SEXP rho);

// Evaluate a function f that takes a single argument x
// x is a vector of length n.  the function returns a
// vector ret of length m.  Evaluate in environment rho
int feval_n(double *x, size_t n, double *ret, size_t m, SEXP f, SEXP rho);

// Evaluate a function f that takes arguments x and y of
// length i and j and returns a vector ret of length m 
// Evaluate in environment rho
int feval_ij(double *x, size_t i, 
        double *y, size_t j, 
        double *ret, size_t m, 
        SEXP f, SEXP rho);

// Evaluate a function f that takes arguments x,y and z of
// length i,j and k and returns a vector ret of length m 
// Evaluate in environment rho
int feval_ijk(double *x, size_t i, double *y, size_t j, double *z, size_t k, 
        double *ret, size_t m, SEXP f, SEXP rho);

// Evaluate a function that returns two vectors of integers of length len
// Evaluate in environment rho
// Typically used to get indices of sparse matrices
int feval_idx(int *idx1, int *idx2, size_t len, SEXP f, SEXP rho);


// Get the list element named str from an R list (list) as a SEXP.  
// Return NULL if no such elem
SEXP get_list_element(SEXP list, const char* str); 

#endif
