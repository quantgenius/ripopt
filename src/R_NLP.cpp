#include "R_NLP.h"
#include "R_comm_util.h"
#include "array_utils.h"
#include <algorithm>
#include <cassert>

using namespace Ipopt;

R_NLP::R_NLP(SEXP num_var_S,
             SEXP var_types_S,
             SEXP x_init_S,
             SEXP x_lb_S,
             SEXP x_ub_S,
             SEXP init_bm_low_S,
             SEXP init_bm_high_S,
             SEXP num_nonlin_S,
             SEXP nonlin_idx_S,

             SEXP num_constr_S,
             SEXP constr_type_S,
             SEXP constr_lb_S,
             SEXP constr_ub_S,
             SEXP init_lambda_S,

             SEXP nnz_jac_S,
             SEXP nnz_hess_S,

             SEXP obj_scale_S,
             SEXP x_scale_S,
             SEXP g_scale_S,

             SEXP objFn,
             SEXP gradFn,
             SEXP constrFn,
             SEXP jacFn,
             SEXP jacIdx,
             SEXP hessFn,
             SEXP hessIdx,
             SEXP env,

             SEXP* retval)
{
    num_var  = INTEGER(num_var_S)[0];
    var_type = INTEGER(var_types_S);
    PROTECT(x_init_g = x_init_S);
    x_lb     = REAL(x_lb_S);
    x_ub     = REAL(x_ub_S);
    PROTECT(init_bm_low_g   = init_bm_low_S);
    PROTECT(init_bm_high_g  = init_bm_high_S);

    num_nonlin = INTEGER(num_nonlin_S)[0];
    nonlin_idx = INTEGER(nonlin_idx_S);

    num_constr  = INTEGER(num_constr_S)[0];
    constr_type = INTEGER(constr_type_S);
    constr_lb   = REAL(constr_lb_S);
    constr_ub   = REAL(constr_ub_S);
    PROTECT(init_lambda_g = init_lambda_S);

    nnz_jac  = INTEGER(nnz_jac_S)[0];
    nnz_hess = INTEGER(nnz_hess_S)[0];

    PROTECT(obj_scale_g = obj_scale_S);
    PROTECT(x_scale_g   = x_scale_S);
    PROTECT(g_scale_g   = g_scale_S);

    PROTECT(objFn_g    = objFn);
    PROTECT(gradFn_g   = gradFn);
    PROTECT(constrFn_g = constrFn);
    PROTECT(jacFn_g    = jacFn);
    PROTECT(jacIdx_g   = jacIdx);
    PROTECT(hessFn_g   = hessFn);
    PROTECT(hessIdx_g  = hessIdx);
    PROTECT(env_g      = env);

    retval_ptr         = retval;

    lambda_g       = allocate_array< double >(num_constr+1);

    solution       =  allocate_array< double >(num_var);
    sol_bm_low     =  allocate_array< double >(num_var);
    sol_bm_high    =  allocate_array< double >(num_var);
    sol_constr_val =  allocate_array< double >(num_constr);
    sol_lambda     =  allocate_array< double >(num_constr);

    sol_retval     = Ipopt::INTERNAL_ERROR;
    solved         = false;
}

R_NLP::~R_NLP()
{
    UNPROTECT(1); // x_init_g
    UNPROTECT(1); // init_bm_low_g
    UNPROTECT(1); // init_bm_high_g
    UNPROTECT(1); // init_lambda_g
    UNPROTECT(1); // obj_scale_g
    UNPROTECT(1); // x_scale_g
    UNPROTECT(1); // g_scale_g
    UNPROTECT(1); // objFn_g
    UNPROTECT(1); // gradFn_g
    UNPROTECT(1); // constrFn_g
    UNPROTECT(1); // jacFn_g
    UNPROTECT(1); // jacIdx_g
    UNPROTECT(1); // hessFn_g
    UNPROTECT(1); // hessIdx_g
    UNPROTECT(1); // env

    deallocate_array< double >(lambda_g);

    deallocate_array< double >(solution);
    deallocate_array< double >(sol_bm_low);
    deallocate_array< double >(sol_bm_high);
    deallocate_array< double >(sol_constr_val);
    deallocate_array< double >(sol_lambda);
}

/* Method to return some info about the nlp */
bool R_NLP::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                         Index& nnz_h_lag, IndexStyleEnum& index_style)
{
    n           = num_var;
    m           = num_constr;
    nnz_jac_g   = nnz_jac;
    nnz_h_lag   = nnz_hess;
    index_style = Ipopt::TNLP::C_STYLE;

    return true;
}


/** overload this method to return scaling parameters. This is
  *  only called if the options are set to retrieve user scaling.
  *  There, use_x_scaling (or use_g_scaling) should get set to true
  *  only if the variables (or constraints) are to be scaled.  This
  *  method should return true only if the scaling parameters could
  *  be provided.
  */
bool R_NLP::get_scaling_parameters(Number& obj_scaling,
                                   bool& use_x_scaling, Index n,
                                   Number* x_scaling,
                                   bool& use_g_scaling, Index m,
                                   Number* g_scaling)
{
    assert(n == num_var);
    assert(m == num_constr);

    if( (obj_scale_g == R_NilValue) &&
        (x_scale_g   == R_NilValue) &&
        (g_scale_g   == R_NilValue))
    {
        return false;
    }

    if(obj_scale_g != R_NilValue)
    {
        obj_scaling = REAL(obj_scale_g)[0];
    }
    else
    {
        obj_scaling = 1.0;
    }

    if(x_scale_g != R_NilValue)
    {
        double *x_scale = REAL(x_scale_g);
        std::copy(&x_scale[0], &x_scale[n], &x_scaling[0]);
        use_x_scaling = true;
    }
    else
    {
        use_x_scaling = false;
    }

    if(g_scale_g != R_NilValue)
    {
        double *g_scale = REAL(g_scale_g);
        std::copy(&g_scale[0], &g_scale[m], &g_scaling[0]);
        use_g_scaling = true;
    }
    else
    {
        use_g_scaling = false;
    }
    return true;
}

/* overload this method to return the variables linearity
 * (TNLP::Linear or TNLP::NonLinear). The var_types
 * array should be allocated with length at least n. (default implementation
 * just return false and does not fill the array).*/
bool R_NLP::get_variables_linearity(Index n, LinearityType* var_types)
{
    assert(n == num_var);
    for(size_t i = 0; i != num_var; ++i)
    {
        if(0 == var_type[i])
        {
            var_types[i] = TNLP::NON_LINEAR;
        }
        if(1 == var_type[i])
        {
            var_types[i] = TNLP::LINEAR;
        }
    }
    return true;
}

/* overload this method to return the constraint linearity.
 * array should be alocated with length at least n. (default implementation
 * just return false and does not fill the array).*/
bool R_NLP::get_constraints_linearity(Index m, LinearityType* const_types)
{
    assert(m == num_constr);
    for(size_t j = 0; j != num_constr; ++j)
    {
        if(0 == constr_type[j])
        {
            const_types[j] = TNLP::NON_LINEAR;
        }
        if(1 == constr_type[j])
        {
            const_types[j] = TNLP::LINEAR;
        }
    }
    return true;
}


/* Method to return the bounds for the problem */
bool R_NLP::get_bounds_info(Index n, Number* x_l, Number* x_u,
                            Index m, Number* g_l, Number* g_u)
{
    assert(n == num_var);
    assert(m == num_constr);

    std::copy(&x_lb[0], &x_lb[num_var], &x_l[0]);
    std::copy(&x_ub[0], &x_ub[num_var], &x_u[0]);
    std::copy(&constr_lb[0], &constr_lb[num_constr], &g_l[0]);
    std::copy(&constr_ub[0], &constr_ub[num_constr], &g_u[0]);

    return true;
}

/* Method to return the starting point for the algorithm */
bool R_NLP::get_starting_point(Index n, bool init_x, Number* x,
                               bool init_z, Number* z_L, Number* z_U,
                               Index m, bool init_lambda,
                               Number* lambda)
{
    assert(n == num_var);
    assert(m == num_constr);

    if(true == init_x)
    {
        if(x_init_g != R_NilValue)
        {
            double *x_init = REAL(x_init_g);
            std::copy(&x_init[0], &x_init[num_var], &x[0]);
        }
        else
        {
            return false;
        }
    }
    if(true == init_z)
    {
        if((init_bm_low_g != R_NilValue) && (init_bm_high_g != R_NilValue))
        {
            double *init_bm_low  = REAL(init_bm_low_g);
            double *init_bm_high = REAL(init_bm_high_g); 
            std::copy(&init_bm_low[0],  &init_bm_low[num_var],  &z_L[0]);
            std::copy(&init_bm_high[0], &init_bm_high[num_var], &z_U[0]);
        }
        else
        {
            return false;
        }
    }
    if(true == init_lambda)
    {
        if(init_lambda_g != R_NilValue)
        {
            double *init_lambda = REAL(init_lambda_g);
            std::copy(&init_lambda[0], &init_lambda[num_constr], &lambda[0]);
        }
        else
        {
            return false;
        }
    }
    return true;
};

/* Method to return the objective value */
bool R_NLP::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
{
    assert(n == num_var);
    int eval_status =
        feval_n(const_cast< double* >(x), n, &obj_value, 1, objFn_g, env_g);
    return(0 == eval_status);
}

/* Method to return the gradient of the objective */
bool R_NLP::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
{
    assert(n == num_var);
    int eval_status =
        feval_n(const_cast< double* > (x), n, grad_f, n, gradFn_g, env_g);
    return(0 == eval_status);
}

/* Method to return the constraint values */
bool R_NLP::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
{
    assert(n == num_var);
    assert(m == num_constr);
    if(m > 0)
    {
        int eval_status =
            feval_n(const_cast< double* >(x), n, g, m, constrFn_g, env_g);
        return(0 == eval_status);
    }
    return true;
}

/** Method to return:
*   1) The structure of the jacobian (if "values" is NULL)
*   2) The values of the jacobian (if "values" is not NULL)
*/
bool R_NLP::eval_jac_g(Index n, const Number* x, bool new_x,
                       Index m, Index nele_jac, Index* iRow, Index *jCol,
                       Number* values)
{
    assert(n == num_var);
    assert(m == num_constr);
    assert(nele_jac == nnz_jac);

    if(nele_jac > 0)
    {
        if(NULL == values)
        {
            int eval_status = feval_idx(iRow, jCol, nele_jac, jacIdx_g, env_g);
            return(0 == eval_status);
        }
        else
        {
            int eval_status =
                feval_n(const_cast< double* >(x), n, values,
                        nele_jac, jacFn_g, env_g);
            return(0 == eval_status);
        }
    }
    else
    {
        return true;
    }
}

/** Method to return:
*   1) The structure of the hessian of the lagrangian (if "values" is NULL)
*   2) The values of the hessian of the lagrangian (if "values" is not NULL)
*/
bool R_NLP::eval_h(Index n, const Number* x, bool new_x,
                   Number obj_factor, Index m, const Number* lambda,
                   bool new_lambda, Index nele_hess, Index* iRow,
                   Index* jCol, Number* values)
{
    assert(n == num_var);
    assert(m == num_constr);
    assert(nele_hess == nnz_hess);

    if(nele_hess > 0)
    {
        if((NULL != iRow) && (NULL != jCol))
        {
            assert(NULL == values);
            assert(NULL == x);
            assert(NULL == lambda);
            if(R_NilValue != hessIdx_g)
            {
                int eval_status =
                    feval_idx(iRow, jCol, nele_hess, hessIdx_g, env_g);
                return(0 == eval_status);
            }
            else
            {
                return false;
            }
        }
        if((NULL != values) && (NULL != x) && ((0 == m) || (NULL != lambda)) )
        {
            assert(NULL == iRow);
            assert(NULL == jCol);

            if(R_NilValue != hessFn_g)
            {
                lambda_g[0] = obj_factor;

                std::copy(&lambda[0], &lambda[m], &lambda_g[1]);
                int eval_status =
                    feval_ij(const_cast< double* >(x), n,
                            lambda_g, (m+1), values,
                            nele_hess, hessFn_g,
                            env_g);
                return(0 == eval_status);
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        return true;
    }
    return true;
}

/** Intermediate Callback method for the user.  Providing dummy
 *  default implementation.  For details see IntermediateCallBack
 *  in IpNLP.hpp. */
bool R_NLP::intermediate_callback(AlgorithmMode mode,
                                  Index iter, Number obj_value,
                                  Number inf_pr, Number inf_du,
                                  Number mu, Number d_norm,
                                  Number regularization_size,
                                  Number alpha_du, Number alpha_pr,
                                  Index ls_trials,
                                  const IpoptData* ip_data,
                                  IpoptCalculatedQuantities* ip_cq)
{
    //TODO: IMPLEMENT THIS
    return true;

}


/** Methods for quasi-Newton approximation.  If the second
 *  derivatives are approximated by Ipopt, it is better to do this
 *  only in the space of nonlinear variables.  The following
 *  methods are call by Ipopt if the quasi-Newton approximation is
 *  selected.  If -1 is returned as number of nonlinear variables,
 *  Ipopt assumes that all variables are nonlinear.  Otherwise, it
 *  calls get_list_of_nonlinear_variables with an array into which
 *  the indices of the nonlinear variables should be written - the
 *  array has the lengths num_nonlin_vars, which is identical with
 *  the return value of get_number_of_nonlinear_variables().  It
 *  is assumed that the indices are counted starting with 1 in the
 *  FORTRAN_STYLE, and 0 for the C_STYLE. */
Index R_NLP::get_number_of_nonlinear_variables()
{
    return num_nonlin;
}

bool R_NLP::get_list_of_nonlinear_variables(Index num_nonlin_vars, Index* pos_nonlin_vars)
{
    std::copy(&nonlin_idx[0], &nonlin_idx[num_nonlin], &pos_nonlin_vars[0]);
    return true;
}



/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
void R_NLP::finalize_solution(SolverReturn status,
                              Index n, const Number* x,
                              const Number* z_L, const Number* z_U,
                              Index m, const Number* g, const Number* lambda,
                              Number obj_value,
                              const IpoptData* ip_data,
                              IpoptCalculatedQuantities* ip_cq)
{
    sol_retval = status;
    assert(n == num_var);

    // Copy into internal representation
    std::copy(&x[0], &x[n], &solution[0]);
    std::copy(&z_L[0], &z_L[n], &sol_bm_low[0]);
    std::copy(&z_U[0], &z_U[n], &sol_bm_high[0]);

    assert(m == num_constr);
    std::copy(&g[0], &g[m], &sol_constr_val[0]);
    std::copy(&lambda[0], &lambda[m], &sol_lambda[0]);

    sol_objval = obj_value;



    // Copy into R retval


    // Solution
    std::copy(&x[0], &x[n], REAL(*retval_ptr));

    // lower bnd mult
    std::copy(&z_L[0], &z_L[n], ((REAL(*retval_ptr))+num_var));

    // upper bnd mult
    std::copy(&z_U[0], &z_U[n], ((REAL(*retval_ptr))+(2*num_var)));

    // Constraing values
    std::copy(&g[0], &g[m], ((REAL(*retval_ptr))+(3*num_var)));

    // Lambda
    std::copy(&lambda[0], &lambda[m],
        ((REAL(*retval_ptr))+((3*num_var)+num_constr)));

    // Objective function value
    REAL(*retval_ptr)[(3*num_var)+(2*num_constr)]   = obj_value;

    // Optimizer return status
    REAL(*retval_ptr)[(3*num_var)+(2*num_constr)+1] = status;

    solved  = true;
}


