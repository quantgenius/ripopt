#include "R_NLP.h"
#include "R_comm_util.h"
#include <cassert>
#include <algorithm>
#include <IpIpoptApplication.hpp>
#include "ipopt_solver.h"

using namespace Ipopt;

SEXP ipopt_solve(SEXP num_var_S,
                 SEXP var_types_S,
                 SEXP x_init_S,
                 SEXP x_lb_S,
                 SEXP x_ub_S,
                 SEXP init_bm_low_S,
                 SEXP init_bm_high_S,
                 SEXP num_nonlin_S,
                 SEXP nonlin_idx_S,

                 SEXP num_constr_S,
                 SEXP constr_type_S,
                 SEXP constr_lb_S,
                 SEXP constr_ub_S,
                 SEXP init_lambda_S,

                 SEXP nnz_jac_S,
                 SEXP nnz_hess_S,

                 SEXP obj_scale_S,
                 SEXP x_scale_S,
                 SEXP g_scale_S,

                 SEXP objFn,
                 SEXP gradFn,
                 SEXP constrFn,
                 SEXP jacFn,
                 SEXP jacIdx,
                 SEXP hessFn,
                 SEXP hessIdx,
                 SEXP env,

                 SEXP options,
                 SEXP option_types)
{
    return ipopt_solve_cpp(num_var_S, var_types_S, x_init_S,
                           x_lb_S, x_ub_S,
                           init_bm_low_S, init_bm_high_S,
                           num_nonlin_S, nonlin_idx_S,
                           num_constr_S, constr_type_S,
                           constr_lb_S, constr_ub_S,
                           init_lambda_S,
                           nnz_jac_S, nnz_hess_S,
                           obj_scale_S, x_scale_S, g_scale_S,
                           objFn, gradFn, constrFn,
                           jacFn, jacIdx,
                           hessFn, hessIdx, env,
                           options, option_types);
}






SEXP ipopt_solve_cpp(SEXP num_var_S,
                     SEXP var_types_S,
                     SEXP x_init_S,
                     SEXP x_lb_S,
                     SEXP x_ub_S,
                     SEXP init_bm_low_S,
                     SEXP init_bm_high_S,
                     SEXP num_nonlin_S,
                     SEXP nonlin_idx_S,

                     SEXP num_constr_S,
                     SEXP constr_type_S,
                     SEXP constr_lb_S,
                     SEXP constr_ub_S,
                     SEXP init_lambda_S,

                     SEXP nnz_jac_S,
                     SEXP nnz_hess_S,

                     SEXP obj_scale_S,
                     SEXP x_scale_S,
                     SEXP g_scale_S,

                     SEXP objFn,
                     SEXP gradFn,
                     SEXP constrFn,
                     SEXP jacFn,
                     SEXP jacIdx,
                     SEXP hessFn,
                     SEXP hessIdx,
                     SEXP env,

                     SEXP options,
                     SEXP option_types)
{
    size_t num_var    = INTEGER(num_var_S)[0];
    size_t num_constr = INTEGER(num_constr_S)[0];

    SEXP retval;
    PROTECT(retval = allocVector(REALSXP, (3*num_var) + (2*num_constr) + 3));
    REAL(retval)[(3*num_var)+(2*num_constr)+2] = 0;

    SmartPtr< TNLP > r_nlp =
        new R_NLP(num_var_S, var_types_S,
                  x_init_S, x_lb_S, x_ub_S,
                  init_bm_low_S, init_bm_high_S,
                  num_nonlin_S, nonlin_idx_S,
                  num_constr_S, constr_type_S, constr_lb_S, constr_ub_S,
                  init_lambda_S,
                  nnz_jac_S, nnz_hess_S,
                  obj_scale_S, x_scale_S, g_scale_S,
                  objFn, gradFn, constrFn,
                  jacFn, jacIdx,
                  hessFn, hessIdx,
                  env,
                  &retval);

    SmartPtr< IpoptApplication > app = new IpoptApplication();

    if(!set_options(options, option_types, app))
    {
        warning("Could not set options.");
    }

    ApplicationReturnStatus status;
    status = app->Initialize();
    if(status != Solve_Succeeded)
    {
        REAL(retval)[(3*num_var)+(2*num_constr)+2] = -1;
        error("**** Could not initialize IPOPT ****");
    }

    status = app->OptimizeTNLP(r_nlp);

    UNPROTECT(1);
    return retval;
};





bool set_options(SEXP options,
                 SEXP option_types,
                 SmartPtr< IpoptApplication >& app)
{
    size_t num_options = length(options);
    size_t num_option_types = length(option_types);
    assert(num_options == num_option_types);


    bool all_good = true;
    SEXP option_names = getAttrib(options, R_NamesSymbol);

    for(size_t i = 0; i != num_options; ++i)
    {
        std::string option_name = CHAR(STRING_ELT(option_names, i));
        std::replace(&option_name[0],
                     &option_name[option_name.size()],
                     '.', '_');

        SEXP cur_option_value = VECTOR_ELT(options, i);
        int cur_option_type  = INTEGER(option_types)[i];

        if(0 == cur_option_type)
        {
            // Numeric Option
            double dbl_option_value = REAL(cur_option_value)[0];

            if(!app->Options()->SetNumericValue(option_name.c_str(), dbl_option_value))
            {
                all_good = false;
                warning("Could not set numeric option %s to value %f.",
                        option_name.c_str(),
                        dbl_option_value);
            }
        }
        if(1 == cur_option_type)
        {
            // Integer Option
           int int_option_value = INTEGER(cur_option_value)[0];

            if(!app->Options()->SetIntegerValue(option_name.c_str(),
                                                int_option_value))
            {
                all_good = false;
                warning("Could not set integer option %s to value %d.",
                        option_name.c_str(), int_option_value);
            }
        }
        if(2 == cur_option_type)
        {
            // String option
            std::string str_option_value =
                CHAR(STRING_ELT(cur_option_value, 0));
            std::replace(&str_option_value[0],
                         &str_option_value[str_option_value.size()],
                         '.', '_');

            if(!app->Options()->SetStringValue(option_name.c_str(),
                                               str_option_value.c_str()))
            {
                all_good = false;
                warning("Could not set string option %s to value %s.",
                        option_name.c_str(), str_option_value.c_str());
            }
        }
    }
    return all_good;
}







